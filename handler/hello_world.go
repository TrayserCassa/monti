package handler

import (
	"fmt"
	"net/http"
)

func HandleHelloWorld(w http.ResponseWriter, r *http.Request) {
	_, err := fmt.Fprintf(w, "Hello World!", r.URL.Path[1:])
	if err != nil {
		fmt.Println("error in handle HandleHelloWorld", err.Error())
	}
}


package main

import (
	"fmt"
	"net/http"
	"os"
)

var identifier string

func main() {
	http.HandleFunc("/", HandleHelloWorld)

	identifier = os.Args[1]

	fmt.Println("Serve at 8080")
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func HandleHelloWorld(w http.ResponseWriter, r *http.Request) {
	_, err := fmt.Fprintf(w, "Hello World from %s!", identifier)
	if err != nil {
		fmt.Println("error in HandleHelloWorld", err.Error())
	}
}

